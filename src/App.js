import { OidcClient } from 'oidc-client-ts';
import { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import './App.css';

import ReactJson from 'react-json-view'
import caseData from './caseData';

// constants
const domain = localStorage.getItem('postelo-domain') || 'pprd1.postelo.fr';
const authServer = localStorage.getItem('postelo-auth-server') || `https://www.${domain}`;
const authority = `${authServer}/oauth2/`;
const apiRootUrl = localStorage.getItem('postelo-api-root') || `https://api.${domain}/v2`;
const origin = window.location.origin;
const logoutUrl = `${authServer}/logout/?continue=${origin}`;
const redirectUrl = `${origin}/redirect`;
// const iframeRedirectUrl = `${origin}/redirect2`;
const windowRedirectUrl = `${origin}/redirect3`;


// logic
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


function storeClientId(clientId) {
  localStorage.setItem('clientId', clientId);
}


function getClientId() {
  return localStorage.getItem('clientId');
}


function storeEndpointUrl(url) {
  localStorage.setItem('endpointUrl', url);
}


function getEndpointUrl() {
  return localStorage.getItem('endpointUrl');
}


function storeToken(token) {
  localStorage.setItem('access_token', token);
}


function getToken() {
  return localStorage.getItem('access_token');
}


function forgetToken() {
  localStorage.removeItem('access_token');
}


function alertError(error) {
  console.log(error);
  alert("❌ An error occured during authentication! See console for more information.");
}


function openExternalWindow(url) {
  const popupSettings = 'popup=1,width=420,height=660,top=50,left=400';
  return window.open(url, '', popupSettings);
}


function startAuthenticationFLow() {
  const tempOidcClient = new OidcClient({
    authority: authority,
    client_id: getClientId(),
    redirect_uri: redirectUrl,
  });

  tempOidcClient.createSigninRequest({}).then(signinRequest => {
    window.location.assign(signinRequest.url);  // you may use window.location.replace instead in actual app
  }).catch(error => alertError(error));
}


// function startIframeAuthenticationFLow() {
//   const iframe = document.getElementById('iframe');

//   const tempOidcClient = new OidcClient({
//     authority: authority,
//     client_id: getClientId(),
//     redirect_uri: iframeRedirectUrl,
//   });

//   tempOidcClient.createSigninRequest({}).then(signinRequest => {
//     iframe.src = signinRequest.url;
//   });
// }


function startWindowAuthenticationFLow() {
  const tempOidcClient = new OidcClient({
    authority: authority,
    client_id: getClientId(),
    redirect_uri: windowRedirectUrl,
  });

  tempOidcClient.createSigninRequest({}).then(signinRequest => {
    openExternalWindow(signinRequest.url);
  }).catch(error => alertError(error));
}


function logout() {
  forgetToken();
  window.location.assign(logoutUrl);            // you may use window.location.replace instead in actual app
}


// function iframeLogout() {
//   const iframe = document.getElementById('iframe');
//   forgetToken();
//   iframe.src = logoutUrl;
//   sleep(150).then(() => iframe.src = "about:blank");
// }


function windowLogout() {
  const externalWindow = openExternalWindow(logoutUrl);
  forgetToken();
  sleep(1000).then(() => externalWindow.close());
}


function completeAuthentication(redirectUri) {
  const oidcClient = new OidcClient({
    authority: authority,
    client_id: getClientId(),
    redirect_uri: redirectUri,
  });

  return oidcClient.processSigninResponse(window.location.href).then(signinResponse => {
    console.log(signinResponse);
    storeToken(signinResponse.access_token);
    alert("✅ Cheers mate, you are authenticated! 🍻");
  }).catch(error => alertError(error));
}


// style
const flexContainer = {
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'stretch',
  position: 'relative',
};


const flexItem1 = {
  flex: '1 1 auto',
};


// const flexIframe = {
//   flex: '0 0 400px',
// };


// const iframeStyle = {
//   width: '100%',
//   height: '700px',
// };


const clientIdInputStyle = {
  display: 'block', 
  maxWidth: '90%', 
  width: '500px',
};


const endpointInputStyle = {
  display: 'block',
  width: '100%',
};


const code = {
  backgroundColor: 'black',
  color: 'lightgray',
  maxWidth: '700px',
  overflowX: 'auto',
};


// components
function Title() {
  return <h1>Postelo - OpenID Authentication Example in a React SPA</h1>
}


function LoginButton() {
  return (
    <button onClick={startAuthenticationFLow}>
      Start Authentication Flow (main window)
    </button>
  );
}


function LogoutButton() {
  return (
    <button onClick={logout}>
      Logout (main window)
    </button>
  );
}


function WindowLoginButton() {
  return (
    <button onClick={startWindowAuthenticationFLow}>
      Start Authentication Flow (external window)
    </button>
  );
}


function WindowLogoutButton() {
  return (
    <button onClick={windowLogout}>
      Logout (external window)
    </button>
  );
}


// function IframeLoginButton() {
//   return (
//     <button onClick={startIframeAuthenticationFLow}>
//       Start Authentication Flow (iframe)
//     </button>
//   );
// }


// function IframeLogoutButton() {
//   return (
//     <button onClick={iframeLogout}>
//       Logout (iframe)
//     </button>
//   );
// }


class ClientIdInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: getClientId() || '',
    };
  }

  updateValue(e) {
    const value = e.target.value;
    this.setState({
      value: value,
    });
    storeClientId(value);
  }

  render() {
    return (
      <label>
        Client ID (<code>client_id</code>)
        <input
          type="text" 
          value={this.state.value}
          onChange={e => this.updateValue(e)}
          style={clientIdInputStyle}
        />
      </label>
    );
  }
}


class EndpointInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: getEndpointUrl() || '',
    };
  }

  updateValue(e) {
    const value = e.target.value;
    this.setState({
      value: value,
    });
    storeEndpointUrl(value);
  }

  render() {
    return (
      <label>
        Endpoint URL
        <input
          type="url" 
          value={this.state.value}
          onChange={e => this.updateValue(e)}
          style={endpointInputStyle}
        />
      </label>
    );
  }
}


function MainHtml() {
  return (
    <div>
      <div>
        <Title />
        <p>
          This example app shows how to authenticate a user in a 
          Single Page Application (SPA) in which no secret can be used.
        </p>
        <h2>Test authentication</h2>
        <p>
          <ClientIdInput />
        </p>
        <h3>In main page</h3>
        <p>
          <LoginButton />
        </p>
        <p>
          <LogoutButton />
        </p>
        <h3>In external window</h3>
        <p>
          <WindowLoginButton />
        </p>
        <p>
          <WindowLogoutButton />
        </p>
        <h2>Test the API</h2>
        <ul>
          <li>Use the <a href='/test'>GET test page</a> to test sending a GET request to the API.</li>
          <li>Use the <a href='/flow'> POST test page</a> to test sending a POST request to the API.</li>
        </ul>
        <h2>How it works</h2>
        <p>
          We use OpenID Connect with the Authorization Code Flow for public 
          clients. It can be used for web apps, servers, and mobile apps.
        </p>
        <p>
          The process is the following:
        </p>
        <ol>
          <li>
            the user agent (UA) generates the code request URL and redirects 
            to the authentication server
          </li>
          <li>
            the user logs in (or is already logged in)
          </li>
          <li>
            the authentication server redirects to the <code>redirect_uri</code> provided 
            by the UA with the <code>code</code> in the URL
          </li>
          <li>
            the UA validates other security parameterts (<code>state</code>, etc.)
          </li>
          <li>
            the UA requests an <code>access_token</code> in exchange for the code. A <code>refresh_token</code> is also included in the response.
          </li>
          <li>
            all the requests to the API can then be authenticated with the token.
          </li>
        </ol>
        {/* <h3>Authentication in iframe (deprecated)</h3>
        <p>
          <IframeLoginButton />
        </p>
        <p>
          <IframeLogoutButton />
        </p> */}
      </div>
    </div>
  );
}


// function Iframe() {
//   return (
//     <iframe 
//       id="iframe"
//       style={iframeStyle}
//       title="Authentication with the Postelo server"
//     >
//     </iframe>
//   )
// }


// routes
function RedirectRoute() {
  completeAuthentication(redirectUrl);
  return <Redirect to="/test" />;
}


function WindowRedirectRoute() {
  completeAuthentication(windowRedirectUrl).then(() => {
    // this code runs in the external window
    // close the external window
    sleep(1000).then(() => window.close());
  });
  return;
}


// function IframeRedirectRoute() {
//   completeAuthentication(iframeRedirectUrl).then(() => {
//     // this code runs in the iframe
//     // trigger a redirection of the main window to the /test route
//     // sleep is a quick and dirty way to handle concurrency: the authentication 
//     // should be completed before the context is killed by window.parent.location.href = ...
//     sleep(500).then(() => window.parent.location.href = '/test');
//   })
//   return;
// }


class APITestRoute extends Component {
  defaultEndpoint = `${apiRootUrl}/cases`;

  constructor(props) {
    super(props);
    this.state = {
      json: null,
    };
  }

  fetchData = () => {
    const authHeader = `Bearer ${getToken()}`;
    const endpoint = getEndpointUrl() || this.defaultEndpoint;

    fetch(endpoint, {
      headers: {
        'Accept': 'application/json',
        'Authorization': authHeader,
      }
    }).then(response => {
      console.log(response);
      if (response.status === 200) {
        alert("✅ The request was properly authenticated!");
      } else {
        alert(`❌ An error occured (${response.status}): are you authenticated?`);
      }
      return response.json();
    }).then(jsonResponse => {
      console.log(jsonResponse);
      const prettyJson = JSON.stringify(jsonResponse, null, 2);
      this.setState({ json: prettyJson });
    });
  }

  render() {
    return (
      <div>
        <Title />
        <p>
          <a href='/'>Home</a>
        </p>
        <p>
          <WindowLoginButton />
        </p>
        <p>
          <WindowLogoutButton />
        </p>
        <h2>Sample data from the API</h2>
        <EndpointInput />
        <p>
          <button onClick={this.fetchData}>
            Fetch data
          </button>
        </p>
        <pre style={code}>
          {this.state.json}
        </pre>
      </div>
    );
  }
}

class ExpertSelectDropdown extends Component {
  count = 10
  endpoint = `${apiRootUrl}/experts?page_size=${this.count}`

  constructor(props) {
    super(props)
    this.state = {
      collection: [],
      value: '',
    } 
  }

  componentDidMount = () => {
    const authHeader = `Bearer ${getToken()}`;

    fetch(this.endpoint, {
      headers: {
        'Accept': 'application/json',
        'Authorization': authHeader,
      }
    }).then(response => {
      return response.json()
    }).then(responseJSON => {
      let collection = []
      responseJSON.results.forEach( expert => {
        collection.push({id: expert.id, value: expert.name})
      });
      this.setState({collection: collection})
    })
  }

  handleChange = (event) => {
    this.props.onChange(event)
  }
  
  render() {
    return (
      <select onChange={this.handleChange}>
        <option defaultValue>Select an expert</option>
        {this.state.collection.map((item, index) => (
          <option key={index} value={item.id}>
            {item.value}
          </option>
        ))}
      </select>
    )
  }
}

class APIFlowRoute extends Component {

  constructor(props) {
    super(props);
    this.state = {
      json: null,
      requestData: caseData,
      responseData: {},
    };
  }

  fetchData = () => {
    
    fetch(`${apiRootUrl}/cases`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`,
      },
      body: JSON.stringify(this.state.requestData)
    }).then(response => {
      return response.json()
    }).then(responseJSON => {
      this.setState({responseData: responseJSON})
    });
  }

  expertSelected = (event) => {
    let updatedCaseData = this.state.requestData
    updatedCaseData.recipient_id = event.target.value
    this.setState({requestData: updatedCaseData})
  }

  render() {
    return (
      <div>
        <Title/>
        <div>
          <div>
            <p>
              Return to <a href='/'> Home</a>
            </p>
            <WindowLoginButton/>
            <WindowLogoutButton/>
            <h3>POST Request Example</h3>
            <p>
              Demonstrate how to do a basic POST request on the cases endpoint to create a new case.
              <br/>
              By default it creates a new case with a message containing an inline attachment
            </p>
          </div>
          <button onClick={this.fetchData}>Create a new case</button>

          <ExpertSelectDropdown onChange={this.expertSelected}/>
          <div className='equal-split'>
            <div>
              <span>Request body</span>
              <ReactJson
                src={this.state.requestData}
                collapsed={false}
                shouldCollapse={false}
                enableClipboard={false}
                displayDataTypes={false}
                displayObjectSize={false}
                theme='harmonic'
                onEdit={ e => {
                  this.setState({requestData: e.updated_src});
                }}
                onDelete={e => {
                  this.setState({requestData: e.updated_src});
                }}
                indentWidth={2}
              />
            </div>
            <div>
              <span>Response:</span>
              <ReactJson
                src={this.state.responseData}
                collapsed={false}
                shouldCollapse={false}
                // enableClipboard={false}
                displayDataTypes={false}
                displayObjectSize={false}
                theme='harmonic'
                indentWidth={2}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}


function HomeRoute() {
  return (
    <div style={flexContainer}>
      <div style={flexItem1}>
        <MainHtml />
      </div>
      {/* <div style={flexIframe}>
        <Iframe />
      </div> */}
    </div>
  );
}


// React app
function App() {
  return (        
    <Router>
      <Switch>
        <Route path="/flow">
          <APIFlowRoute/>
        </Route>
        <Route path="/test">
          <APITestRoute />
        </Route>
        <Route path="/redirect">
          <RedirectRoute />
        </Route>
        {/* <Route path="/redirect2">
          <IframeRedirectRoute />
        </Route> */}
        <Route path="/redirect3">
          <WindowRedirectRoute />
        </Route>
        <Route path="/" >
          <HomeRoute />
        </Route>
      </Switch>
    </Router>
  );
}


export default App;
