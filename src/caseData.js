const caseData = {
    "recipient_id": "expert_7",
    "patient": {
        "first_name": "Jane",
        "last_name": "Doe",
        "birth_date": "2022-01-01",
        "french_NIR": "000000000000097",
        "sex": 2,
        "consent": true
    },
    "subject": "Test POST case",
    "message": {
      "body" : "Message body",
      "attachments": [
          {
              'type': 'inline',
              'name': 'hello-world.txt',
              'content': 'SGVsbG8gd29ybGQ=',
          },
      ],
  },
    "deadline": "2023-01-01T23:59:59Z"
};

export default caseData;
