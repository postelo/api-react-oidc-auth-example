# Postelo API - OpenID Authentication Example in a React SPA

This example application shows how to authenticate a user using Postelo's OpenID Connect authentication server in a React application.

## Quickstart

### Get your API credentials

If you don't already have any API credentials you can fill in <a href="https://docs.google.com/forms/d/e/1FAIpQLSduM0mQWAyD1_e67PcnXPdDACp5OajINJgaw93eZ8pspmPGig/viewform?usp=pp_url&entry.781206806=Test" target="_blank" rel="noopener">this form</a> to receive them

**Note: The following redirect URIs must be explicitly whitelisted:**
- http://localhost:3000/redirect 
<!-- - http://localhost:3000/redirect2 -->
- http://localhost:3000/redirect3

### Create a test user

For this example app you will need a user account to login with.  
If you don't already have one,you can create it from here: https://www.pprd1.postelo.fr/join/

### Clone, install and run the example app

First clone the example project from our Gitlab public repository
```
git clone https://gitlab.com/postelo/api-react-oidc-auth-example.git
```

Then execute the following commands to get you started
```
cd api-react-oidc-auth-example/
npm install --force
npm start
```
Requires `--force` when installing modules in order for the json viewer to work

### Authenticate your user...

#### ...in main window

This is probably not what you want to do in your app, but this is convenient to understand the process by observing the urls and playing with them (e.g. tempering with the `state` or other security parameters):

1. Browse to <a href="http://localhost:3000" target="_blank" rel="noopener">http://localhost:3000</a>
1. Copy your `client_id` in the "Client ID" input
1. Click the "Start Authentication Flow (main window)" button to test authentication in the main window
1. Authenticate with your test user: you should be redirected to http://localhost:3000/authenticated
1. Click the "Fetch data" button and check that the API data is printed.

**Note: you may get an error alert before the alert confirming that you are authenticated. This is because the React route is rendered twice, and the second authentication fails (because the `state` has already been consumed by the first call).**

**In you implementation, you should make sure that the request which exchanges the `code` for an `access_token` is not called twice.**

#### ...in an external window

Now you can try the same in an external window:
1. Stay on http://localhost:3000/authenticated
1. Click the "Logout (external window)" button
1. Click the "Fetch data" button and check that you get an error
1. Click the "Start Authentication Flow (external window)" button
1. Authenticate your user
1. Click the "Fetch data" button and check that the API data is printed.

**Note: this is probably what you want to do in an actual app as this is better in terms of user experience.**

#### ...in an iframe (deprecated)

Now you can try the same in an iframe:
1. Stay on http://localhost:3000/authenticated
1. Click the "Logout (iframe)" button
1. Click the "Fetch data" button and check that you get an error
1. Click "Start Authentication Flow (iframe)"
1. Authenticate your user in the iframe
1. Click the "Fetch data" button and check that the API data is printed.

## How it works

We use the OpenID Connect Authentication Code flow for a pulic client (i.e. no secret key is used). Security is based on the `redirect_uri` which must be pre-registered with the authentication server, and other security measures (`state`, PKCE, etc.).

More information on the Authentication Code flow can be found on <a href="https://openid.net/specs/openid-connect-core-1_0.html#CodeFlowSteps" target="_blank" rel="noopener">the official OpenID Connect website</a>.

### `oidc-client-ts`

In this app, most of the OpenID Connect logic (including security) is implemented by
<a href="https://github.com/authts/oidc-client-ts" target="_blank" rel="noopener">`oidc-client-ts`</a>.

See its documentation at the follawing address: <a href="https://authts.github.io/oidc-client-ts/" target="_blank" rel="noopener">https://authts.github.io/oidc-client-ts/</a>

`oidc-client-ts` is a TypeScript port of 
<a href="https://github.com/IdentityModel/oidc-client-js" target="_blank" rel="noopener">`oidc-client-js`</a>
which is listed as a Certifed OpenID Connect Implementation on
<a href="https://openid.net/developers/certified/" target="_blank" rel="noopener">the official OpenID Connect website</a>.

The three main elements of `oidc-client-ts` which are used in this app are:
1. `OidcClient`: the OpenID client object
```js
import { OidcClient } from 'oidc-client-ts';

[...]

const oidcClient = new OidcClient({
  authority: [...],
  client_id: [...]
  redirect_uri: [...]
});
```
2. `oidcClient.createSigninRequest({})` which generates the client-side security elements (`state`, PKCE data etc.), stores them in the `localStorage` and outputs the redirect URL used to request the `code`
3. `oidcClient.processSigninResponse(window.location.href)` which reads the `code` (and other data) returned from the authentication server, performs all the security validations, and posts a request to exchange the obtained `code` for an `acess_token` and `refresh_token`.

<a href="https://github.com/authts/oidc-client-ts" target="_blank" rel="noopener">`oidc-client-ts`</a>
also includes higher level object and methods to manage session data, refresh the token, etc. It makes extensive use of the `localStorage` which must be accessible.

## Configuration

The app is configured to work with the test servers https://www.pprd1.postelo.fr (authentication server) and https://api.pprd1.postelo.fr/v2 (api).

This default behavior can be modified by settings the following `localStorage` variables:

```js
localStorage.setItem('postelo-domain', 'demo.postelo.fr');
/*
=> https://www.demo.postelo.fr as the authentication server
=> https://api.demo.postelo.fr/v2 as the API root URL
*/

// or define each server, e.g.
localStorage.setItem('postelo-auth-server', 'http://localhost:8000');
/*
=> http://localhost:8000 as the authentication server
*/

localStorage.setItem('postelo-api-root', 'http://localhost:8000/v2');
/*
=> http://localhost:8000/v2 as the API root URL
*/
```

A hard refresh of the React app is needed for these variables to be taken into account.

## Credits

This app was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

© 2022 Postelo

Postelo, SAS au capital de 10 520 €, immatriculée sous le numéro 837 590 967 au registre du commerce et des sociétés de Toulouse

Siège social : 13 rue Sainte Ursule, 31000 Toulouse, FRANCE

Email : hello@postelo.fr

Tel. : +33 (0)5 67 81 13 15
